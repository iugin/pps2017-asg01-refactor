package model.saves;

import model.Interfaces.*;

import java.io.File;

public class GameConfigurationFactory {

    private static final String RES_DIR = "res";
    private static final String FILE_VISITORS = "Visitors.txt";
    private static final String FILE_GUARDS = "GuardsUserPass.txt";
    private static final String FILE_CELLS = "Cells.txt";
    private static final String FILE_PRISONERS = "Prisoners.txt";
    private static final String FILE_CURRENT_PRISONERS = "CurrentPrisoners.txt";
    private static final String FILE_MOVEMENTS = "Movements.txt";

    private static GameConfiguration<Visitor> visitorsGameConfiguration = new FileGameConfiguration<>(new File(RES_DIR, FILE_VISITORS));
    private static GameConfiguration<Guard> guardsGameConfiguration = new FileGameConfiguration<>(new File(RES_DIR, FILE_GUARDS));
    private static GameConfiguration<Cell> cellsGameConfiguration = new FileGameConfiguration<>(new File(RES_DIR, FILE_CELLS));
    private static GameConfiguration<Prisoner> prisonersGameConfiguration = new FileGameConfiguration<>(new File(RES_DIR, FILE_PRISONERS));
    private static GameConfiguration<Prisoner> currentPrisonersGameConfiguration = new FileGameConfiguration<>(new File(RES_DIR, FILE_CURRENT_PRISONERS));
    private static GameConfiguration<Movement> movementsGameConfiguration = new FileGameConfiguration<>(new File(RES_DIR, FILE_MOVEMENTS));

    private GameConfigurationFactory() {
    }

    public static GameConfiguration<Visitor> getVisitorsGameConfiguration() {
        return visitorsGameConfiguration;
    }

    public static GameConfiguration<Guard> getGuardsGameConfiguration() {
        return guardsGameConfiguration;
    }

    public static GameConfiguration<Cell> getCellsGameConfiguration() {
        return cellsGameConfiguration;
    }

    public static GameConfiguration<Prisoner> getPrisonersGameConfiguration() {
        return prisonersGameConfiguration;
    }

    public static GameConfiguration<Prisoner> getCurrentPrisonersGameConfiguration() {
        return currentPrisonersGameConfiguration;
    }

    public static GameConfiguration<Movement> getMovementsGameConfiguration() {
        return movementsGameConfiguration;
    }
}
