package model.saves;

import java.io.*;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class FileGameConfiguration<T> implements GameConfiguration<T> {

    private final File file;

    public FileGameConfiguration(File file) {
        this.file = file;
    }

    @Override
    public List<T> readConfiguration() {
        try {
            return FileManager.readObjectList(file);
        } catch (IOException | ClassNotFoundException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public void writeConfiguration(Iterator<? extends T> visitors) {
        try {
            FileManager.writeObjectList(file, visitors);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
