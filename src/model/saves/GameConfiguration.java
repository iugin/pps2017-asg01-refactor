package model.saves;

import java.util.Iterator;
import java.util.List;

public interface GameConfiguration<T> {

    List<T> readConfiguration();

    void writeConfiguration(final Iterator<? extends T> visitors);
}
