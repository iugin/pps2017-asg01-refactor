package model.saves;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FileManager {

    public static <T> List<T> readObjectList(File file) throws IOException, ClassNotFoundException {
        List<T> list = new ArrayList<>();
        if (file.exists() && file.length() != 0) {
            FileInputStream fi = null;
            ObjectInputStream oi = null;
            try {
                fi = new FileInputStream(file);
                oi = new ObjectInputStream(fi);
                while (true) {
                    list.add((T) oi.readObject());
                }
            } catch (EOFException e) {
            } finally {
                try {
                    oi.close();
                    fi.close();
                } catch (NullPointerException | IOException e) {
                }
            }
        }
        return list;
    }

    public static <T> void writeObjectList(File file, Iterator<T> list) throws IOException {
        FileOutputStream fo = null;
        ObjectOutputStream os = null;
        try {
            fo = new FileOutputStream(file);
            os = new ObjectOutputStream(fo);
            os.flush();

            while (list.hasNext()) {
                os.writeObject(list.next());
            }
        } finally {
            if (os != null) {
                os.close();
            }
            if (fo != null) {
                fo.close();
            }
        }
    }
}
