package model.datamanager;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public interface ObjectManager<O> {

    void add(O object);

    void addAll(List<O> objects);

    void remove(O object);

    List<O> getList();

    List<O> getList(Predicate<O> predicate);

    void updateList(Function<O, O> function);
}
