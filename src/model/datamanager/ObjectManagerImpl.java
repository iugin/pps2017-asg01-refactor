package model.datamanager;

import model.saves.GameConfiguration;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ObjectManagerImpl<O> implements ObjectManager<O> {

    final GameConfiguration<O> gameConfiguration;

    public ObjectManagerImpl(GameConfiguration<O> gameConfiguration) {
        this.gameConfiguration = gameConfiguration;
    }

    @Override
    public void add(O object) {
        List<O> tmp = gameConfiguration.readConfiguration();
        tmp.add(object);
        gameConfiguration.writeConfiguration(tmp.iterator());
    }

    @Override
    public void addAll(List<O> objects) {
        List<O> tmp = gameConfiguration.readConfiguration();
        tmp.addAll(objects);
        gameConfiguration.writeConfiguration(tmp.iterator());
    }

    @Override
    public void remove(O object) {
        List<O> tmp = gameConfiguration.readConfiguration();
        tmp.remove(object);
        gameConfiguration.writeConfiguration(tmp.iterator());
    }

    @Override
    public List<O> getList() {
        return gameConfiguration.readConfiguration();
    }

    @Override
    public List<O> getList(Predicate<O> predicate) {
        return getList().stream().filter(predicate).collect(Collectors.toList());
    }

    @Override
    public void updateList(Function<O, O> function) {
        gameConfiguration.writeConfiguration(getList().stream().map(function).iterator());
    }
}
