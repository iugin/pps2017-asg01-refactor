package model.datamanager;

import model.Interfaces.*;
import model.saves.GameConfigurationFactory;

public class DataManagerFactory {
    
    private DataManagerFactory() {
    }

    public static ObjectManager<Visitor> getVisitorsDataManager() {
        return new ObjectManagerImpl<>(GameConfigurationFactory.getVisitorsGameConfiguration());
    }

    public static ObjectManager<Guard> getGuardsDataManager() {
        return new ObjectManagerImpl<>(GameConfigurationFactory.getGuardsGameConfiguration());
    }

    public static ObjectManager<Cell> getCellsDataManager() {
        return new ObjectManagerImpl<>(GameConfigurationFactory.getCellsGameConfiguration());
    }

    public static ObjectManager<Prisoner> getPrisonersDataManager() {
        return new ObjectManagerImpl<>(GameConfigurationFactory.getPrisonersGameConfiguration());
    }

    public static ObjectManager<Prisoner> getCurrentPrisonersDataManager() {
        return new ObjectManagerImpl<>(GameConfigurationFactory.getCurrentPrisonersGameConfiguration());
    }

    public static ObjectManager<Movement> getMovementsDataManager() {
        return new ObjectManagerImpl<>(GameConfigurationFactory.getMovementsGameConfiguration());
    }
}
