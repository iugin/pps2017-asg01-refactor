package model.Interfaces;

public interface Movement {

    /**
     * ritorna la descrizione del movimento
     *
     * @return description of the movement
     */
    String getDescr();

    /**
     * setta la descrizione del movimento
     *
     * @param description of the movement
     */
    void setDescr(String description);

    /**
     * ritorna la quantita del movimento
     *
     * @return amount of the movement
     */
    double getAmount();

    /**
     * setta la quantit� del movimento
     *
     * @param amount of the movement
     */
    void setAmount(double amount);

    /**
     * ritorna se il movimento � positivo o negativo
     *
     * @return the sign of the movement (+ or -)
     */
    char getChar();

    /**
     * ritorna la data del movimento
     *
     * @return the date of the movement
     */
    String getData1();

}
