package model.Interfaces;

import java.util.Date;
import java.util.List;

/**
 * interfaccia che rappresenta un prigioniero
 */
public interface Prisoner extends Person {

    /**
     * Metodo che aggiunge un crimine alla lista dei crimini
     *
     * @return
     */
    void addCrime(String crimine);

    /**
     * Metodo che ritrna la lista dei crimini commessi da un criminale
     *
     * @return la lista dei crimini
     */
    List<String> getCrimini();

    /**
     * Metodo che ritorna l'id del prigioniero
     *
     * @return id del prigioniero
     */
    int getIdPrigioniero();

    /**
     * Metodo che setta l'id del prigioniero
     *
     * @return
     */
    void setIdPrigioniero(int idPrigioniero);

    /**
     * Metodo che ritorna la data di inizio della reclusione
     *
     * @return data di inizio della reclusione
     */
    Date getInizio();

    /**
     * Metodo che setta la data di inizio della reclusione
     *
     * @return
     */
    void setInizio(Date inizio);

    /**
     * Metodo che ritorna la data di fine della reclusione
     *
     * @return data di fine della reclusione
     */
    Date getFine();

    /**
     * Metodo che setta la data di fine della reclusione
     *
     * @return
     */
    void setFine(Date fine);

    /**
     * Metodo che ritorna l'id della cella
     *
     * @return id della cella
     */
    int getCellID();

    /**
     * metodo che setta l'id della cella
     *
     * @param cellID id della cella
     */
    void setCellID(int cellID);
}