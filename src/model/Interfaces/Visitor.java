package model.Interfaces;

/**
 * questa interfaccia rappresenta un visitatore
 */
public interface Visitor extends Person {

    /**
     * metodo che ritorna l'id del prigioniero che il visitatore è andato a trovare
     *
     * @return id del prigioniero visitato
     */
    int getPrisonerID();

    /**
     * metodo che imposta l'id del prigioniero
     *
     * @param prisonerID id del prigioniero
     */
    void setIdPrisoner(int prisonerID);
}
