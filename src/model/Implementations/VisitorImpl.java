package model.Implementations;

import java.util.Date;
import java.util.Objects;

import model.Interfaces.Visitor;

/**
 * Implementazione di un visitatore
 */
public class VisitorImpl extends PersonImpl implements Visitor {

    /**
     *
     */
    private static final long serialVersionUID = 5306827736761721189L;

    /**
     * id del prigioniero
     */
    private int prisonerID;

    /**
     * @param name       nome
     * @param surname    cognome
     * @param birthDate  data di nascita
     * @param prisonerID id del prigioniero andato a trovare
     */
    public VisitorImpl(String name, String surname, Date birthDate, int prisonerID) {
        super(name, surname, birthDate);
        this.prisonerID = prisonerID;
    }

    @Override
    public int getPrisonerID() {
        return this.prisonerID;
    }

    @Override
    public void setIdPrisoner(int prisonerID) {
        this.prisonerID = prisonerID;
    }

    @Override
    public String toString() {
        return "VisitorImpl getName()=" + getName() + ", getSurname()=" + getSurname() + ", getBirthDate()=" + getBirthDate() + "[getIdPrisoner()=" + getPrisonerID() + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisitorImpl visitor = (VisitorImpl) o;
        return prisonerID == visitor.prisonerID && getName().equals(visitor.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(prisonerID + getName());
    }
}
