package model.Implementations;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

import model.Interfaces.Movement;

/**
 * implementazione di un movimento
 */
public class MovementImpl implements Serializable, Movement {

    /**
     *
     */
    private static final long serialVersionUID = -8676396152502823263L;

    private String descr;
    private double amount;
    private String data1;
    private char ch;

    /**
     * costruttore di movimento
     *
     * @param descr  descrizione
     * @param amount ammontare
     * @param ch     segno
     */
    public MovementImpl(String descr, double amount, char ch) {
        this.amount = amount;
        this.descr = descr;
        SimpleDateFormat data = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        data1 = data.format(c.getTime());
        this.ch = ch;
    }

    @Override
    public String getDescr() {
        return this.descr;
    }

    @Override
    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Override
    public double getAmount() {
        return this.amount;
    }

    @Override
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public char getChar() {
        return this.ch;
    }

    @Override
    public String getData1() {
        return this.data1;
    }

    public String toString() {
        return "Movimento [descr=" + descr + "segno" + ch + ", amount=" + amount + ", data=" + data1 + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovementImpl movement = (MovementImpl) o;
        return Double.compare(movement.amount, amount) == 0 &&
                ch == movement.ch &&
                Objects.equals(descr, movement.descr) &&
                Objects.equals(data1, movement.data1);
    }

    @Override
    public int hashCode() {

        return Objects.hash(descr, amount, data1, ch);
    }
}
