package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.Login;

public class LoginView extends PrisonManagerJFrame implements Login {

    /**
     *
     */
    private static final long serialVersionUID = -9055948983228935131L;

    private final PrisonManagerJPanel south;
    private final JButton login = new JButton("Login");
    private final PrisonManagerJPanel center;
    private final JLabel username = new JLabel("Username");
    private final JTextField username1 = new JTextField(8);
    private final JLabel password = new JLabel("Password");
    private final JPasswordField password1 = new JPasswordField(8);
    private final PrisonManagerJPanel north;
    private final JLabel title = new JLabel("Prison Manager");

    /**
     * costruttore
     */
    public LoginView() {
        this.setSize(400, 130);
        this.getContentPane().setLayout(new BorderLayout());
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(login);
        this.getContentPane().add(BorderLayout.SOUTH, south);
        center = new PrisonManagerJPanel(new FlowLayout());
        center.add(username);
        center.add(username1);
        center.add(password);
        center.add(password1);
        this.getContentPane().add(BorderLayout.CENTER, center);
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(title);
        this.getContentPane().add(BorderLayout.NORTH, north);
    }

    public void displayErrorMessage(String error) {
        JOptionPane.showMessageDialog(this, error);
    }

    public void addLoginListener(LoginListener loginListener) {
        login.addActionListener((e) -> loginListener.onLoginSubmit(
                username1.getText(),
                new String(password1.getPassword()
                )));
    }

    public void reset() {
        this.username1.setText("");
        this.password1.setText("");
    }
}
