package view.Interfaces.Inter;

import javax.swing.JTable;

public interface ViewVisitors {

    /**
     * ritorna il rank
     *
     * @return il rank
     */
    int getRank();

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    interface BackListener {
        void onBack();
    }

    /**
     * crea la tabella in cui mettere i visitatori e la imposta
     *
     * @param table tabella contenente i dati
     */
    void createTable(JTable table);
}
