package view.Interfaces.Inter;

import controller.Implementations.AddMovementControllerImpl.BackListener;
import controller.Implementations.AddMovementControllerImpl.InsertListener;

public interface AddMovement {

    /**
     * @return the rank
     */
    int getRank();

    /**
     * add the backlistener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    /**
     * add the insertListener
     *
     * @param insertListener
     */
    void addInsertListener(InsertListener insertListener);

    /**
     * @return the description
     */
    String getDesc();

    /**
     * return value of the movement
     *
     * @return the value
     */
    Double getValue();

    /**
     * return the symbol of the movement
     *
     * @return the symbol
     */
    String getSymbol();

    /**
     * display a message
     *
     * @param error the message
     */
    void displayErrorMessage(String error);

}
