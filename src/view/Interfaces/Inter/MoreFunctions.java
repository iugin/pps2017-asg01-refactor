package view.Interfaces.Inter;

import controller.Implementations.MoreFunctionsControllerImpl.AddMovementListener;
import controller.Implementations.MoreFunctionsControllerImpl.AddVisitorsListener;
import controller.Implementations.MoreFunctionsControllerImpl.BackListener;
import controller.Implementations.MoreFunctionsControllerImpl.BalanceListener;
import controller.Implementations.MoreFunctionsControllerImpl.Chart1Listener;
import controller.Implementations.MoreFunctionsControllerImpl.Chart2Listener;
import controller.Implementations.MoreFunctionsControllerImpl.ViewCellsListener;
import controller.Implementations.MoreFunctionsControllerImpl.ViewVisitorsListener;

public interface MoreFunctions {

    /**
     * ritorna il rank
     *
     * @return il rank
     */
    int getRank();

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    /**
     * aggiunge l'add movement listener
     *
     * @param addMovementListener
     */
    void addAddMovementListener(AddMovementListener addMovementListener);

    /**
     * aggiunge il balance listener
     *
     * @param balanceListener
     */
    void addBalanceListener(BalanceListener balanceListener);

    /**
     * aggiunge l'add chart 1 listener
     *
     * @param chart1Listener
     */
    void addChart1Listener(Chart1Listener chart1Listener);

    /**
     * aggiunge l'add chart 2 listener
     *
     * @param chart2Listener
     */
    void addChart2Listener(Chart2Listener chart2Listener);

    /**
     * aggiunge l'add visitors listener
     *
     * @param addVisitorsListener
     */
    void addAddVisitorsListener(AddVisitorsListener addVisitorsListener);

    /**
     * aggiunge l'add view visitors listener
     *
     * @param viewVisitorsListener
     */
    void addViewVisitorsListener(ViewVisitorsListener viewVisitorsListener);

    /**
     * aggiunge il view cells listener
     *
     * @param viewCellsListener
     */
    void addViewCellsListener(ViewCellsListener viewCellsListener);
}
