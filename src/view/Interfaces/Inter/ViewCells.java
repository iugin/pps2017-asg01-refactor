package view.Interfaces.Inter;

import javax.swing.JTable;

import controller.Implementations.ViewCellsControllerImpl.BackListener;

public interface ViewCells {

    /**
     * ritorna il rank
     *
     * @return il rank
     */
    int getRank();

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    /**
     * crea la tabella in cui mettere le celle
     *
     * @param table
     */
    void createTable(JTable table);

}
