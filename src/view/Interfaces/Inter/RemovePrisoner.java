package view.Interfaces.Inter;


import controller.Implementations.RemovePrisonerControllerImpl.BackListener;
import controller.Implementations.RemovePrisonerControllerImpl.RemoveListener;

public interface RemovePrisoner {

    /**
     * aggiunge il remove listener
     *
     * @param removeListener
     */
    void addRemoveListener(RemoveListener removeListener);

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    /**
     * ritorna l'id del prigioniero
     *
     * @return id del prigioniero
     */
    int getID();

    /**
     * mostra un messaggio
     *
     * @param error il messaggio
     */
    void displayErrorMessage(String error);

    /**
     * ritorna il rank
     *
     * @return il rank
     */
    int getRank();
}
