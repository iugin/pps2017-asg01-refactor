package view.Interfaces.Inter;

import controller.Implementations.RemoveGuardControllerImpl.BackListener;
import controller.Implementations.RemoveGuardControllerImpl.RemoveGuardListener;

public interface RemoveGuard {

    /**
     * ritorna l'id
     *
     * @return
     */
    int getID();

    /**
     * mostra un messaggio
     *
     * @param error il messaggio
     */
    void displayErrorMessage(String error);

    /**
     * ritona il rank
     *
     * @return rank
     */
    int getRank();

    /**
     * aggiunge il remove guard listener
     *
     * @param removeGuardListener
     */
    void addRemoveGuardListener(RemoveGuardListener removeGuardListener);

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);
}
