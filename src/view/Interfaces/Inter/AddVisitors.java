package view.Interfaces.Inter;

import controller.Implementations.AddVisitorsControllerImpl.BackListener;
import controller.Implementations.AddVisitorsControllerImpl.InsertListener;
import model.Implementations.VisitorImpl;

public interface AddVisitors {

    /**
     * ritorna il visitatore inserito nella view
     *
     * @return the visitor
     */
    VisitorImpl getVisitor();

    /**
     * ritorna il rank
     *
     * @return il rank
     */
    int getRank();

    /**
     * mostra un messaggio
     *
     * @param error il messaggio
     */
    void displayErrorMessage(String error);

    /**
     * aggiunger il backLsitener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    /**
     * aggiunge l'insert visitor listener
     *
     * @param insertListener
     */
    void addInsertVisitorListener(InsertListener insertListener);
}
