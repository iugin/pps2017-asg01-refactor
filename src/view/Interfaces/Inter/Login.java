package view.Interfaces.Inter;

public interface Login {

    /**
     * mostra un messaggio
     *
     * @param error il messaggio
     */
    void displayErrorMessage(String error);

    /**
     * aggiunge il login listener
     *
     * @param loginListener
     */
    void addLoginListener(LoginListener loginListener);

    interface LoginListener {
        void onLoginSubmit(String username, String password);
    }
}
