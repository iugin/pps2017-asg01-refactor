package view.Interfaces.Inter;

import java.util.Date;

import javax.swing.JTable;

import controller.Implementations.ShowPrisonersControllerImpl.BackListener;
import controller.Implementations.ShowPrisonersControllerImpl.ComputeListener;

public interface ShowPrisoners {

    /**
     * crea la tabella in cui mettere i prigionieri
     *
     * @param table tabella
     */
    void createTable(JTable table);

    /**
     * ritorna il rank
     *
     * @return rank
     */
    int getRank();

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    /**
     * aggiunge il compute listener
     *
     * @param computeListener
     */
    void addComputeListener(ComputeListener computeListener);

    /**
     * ritorna la data iniziale della ricerca
     *
     * @return data iniziale della ricerca
     */
    Date getFrom();

    /**
     * ritorna la data conclusiva della ricerca
     *
     * @return data conclusiva della ricerca
     */
    Date getTo();
}
