package view.Interfaces.Inter;

import controller.Implementations.SupervisorControllerImpl.BackListener;
import controller.Implementations.SupervisorControllerImpl.InsertGuardListener;
import controller.Implementations.SupervisorControllerImpl.RemoveGuardListener;
import controller.Implementations.SupervisorControllerImpl.ShowPrisonersListener;
import controller.Implementations.SupervisorControllerImpl.ViewGuardListener;

public interface SupervisorFunctions {

    /**
     * ritorna il rank
     *
     * @return rank
     */
    int getRank();

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    /**
     * aggiunge lo show prisoners listener
     *
     * @param showPrisonersListeners
     */
    void addShowPrisonersListener(ShowPrisonersListener showPrisonersListeners);

    /**
     * aggiunge l'insert guard listener
     *
     * @param insertGaurdListener
     */
    void addInsertGuardListener(InsertGuardListener insertGaurdListener);

    /**
     * aggiunge il remove guard listener
     *
     * @param removeListener
     */
    void addRemoveGuardListener(RemoveGuardListener removeListener);

    /**
     * aggiunge il view guard listener
     *
     * @param viewListener
     */
    void addviewGuardListener(ViewGuardListener viewListener);
}
