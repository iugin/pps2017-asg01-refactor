package view.Interfaces.Inter;

public interface Main {

    /**
     * aggiunge il logout listener
     *
     * @param logoutListener
     */
    void addLogoutListener(LogoutListener logoutListener);

    interface LogoutListener {
        void onLogout();
    }

    /**
     * aggiunge l'insert prisoner listener
     *
     * @param insertPrisonerListener
     */
    void addInsertPrisonerListener(InsertPrisonerListener insertPrisonerListener);

    interface InsertPrisonerListener {
        void onInsertPrisoner();
    }

    /**
     * aggiunge il remove prisoner listener
     *
     * @param removePrisonerListener
     */
    void addRemovePrisonerListener(RemovePrisonerListener removePrisonerListener);

    interface RemovePrisonerListener {
        void onRemovePrisoner();
    }

    /**
     * aggiunge il view prisoner listener
     *
     * @param viewPrisonerListener
     */
    void addViewPrisonerListener(ViewPrisonerListener viewPrisonerListener);

    interface ViewPrisonerListener {
        void onViewPrisoner();
    }

    /**
     * aggiunge il more functions listener
     *
     * @param moreFListener
     */
    void addMoreFunctionsListener(MoreFunctionsListener moreFListener);

    interface MoreFunctionsListener {
        void onMoreFunctions();
    }

    /**
     * aggiunge il supervisor listener
     *
     * @param supervisorListener
     */
    void addSupervisorListener(SupervisorListener supervisorListener);

    interface SupervisorListener {
        void onSupervisor();
    }
}
