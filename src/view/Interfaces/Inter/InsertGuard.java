package view.Interfaces.Inter;

import controller.Implementations.InsertGuardControllerImpl.BackListener;
import controller.Implementations.InsertGuardControllerImpl.InsertListener;
import model.Interfaces.Guard;

public interface InsertGuard {

    /**
     * metodo che ritorna la guardia inserita
     *
     * @return la guardia
     */
    Guard getGuard();

    /**
     * restituisce il rank
     *
     * @return
     */
    int getRank();

    /**
     * aggiunge l' insert listener
     *
     * @param insertListener
     */
    void addInsertListener(InsertListener insertListener);

    /**
     * mostra un messaggio
     *
     * @param error il messaggio
     */
    void displayErrorMessage(String error);

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);
}
