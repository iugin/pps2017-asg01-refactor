package view.Interfaces.Inter;

import java.util.Date;
import java.util.List;

import controller.Implementations.InsertPrisonerControllerImpl.AddCrimeListener;
import controller.Implementations.InsertPrisonerControllerImpl.BackListener;
import controller.Implementations.InsertPrisonerControllerImpl.InsertPrisonerListener;

public interface InsertPrisoner {

    /**
     * aggiunge l'insert prisoner listener
     *
     * @param addPrisonerListener
     */
    void addInsertPrisonerListener(InsertPrisonerListener addPrisonerListener);

    /**
     * aggiunge il back listener
     *
     * @param backListener
     */
    void addBackListener(BackListener backListener);

    /**
     * ritorna l'id del prigioniero
     *
     * @return l'id
     */
    int getPrisonerID1();

    /**
     * restituisce nome prigioniero
     *
     * @return il nome
     */
    String getName1();

    /**
     * restituisce il cognome del prigioniero
     *
     * @return il cognome
     */
    String getSurname1();

    /**
     * ritorna la data di inizio di prigionia
     *
     * @return data di inizio di prigionia
     */
    Date getStart1();

    /**
     * ritorna la data di fine della prigionia
     *
     * @return data di fine della prigionia
     */
    Date getEnd1();

    /**
     * ritorna data di nascita
     *
     * @return data di nascita
     */
    Date getBirth1();

    /**
     * ritorna l'id della cella
     *
     * @return id della cella
     */
    int getCellID();

    /**
     * mostra un messaggio
     *
     * @param error il messaggio
     */
    void displayErrorMessage(String error);

    /**
     * ritorna il rank
     *
     * @return
     */
    int getRank();

    /**
     * imposta la lista dei crimini nella textarea
     *
     * @param list la lista dei crimini
     */
    void setList(List<String> list);

    /**
     * ritorna la lista dei crimini presente nella textarea
     *
     * @return la lista di crimini
     */
    List<String> getList();

    /**
     * ritorna il crimine presente nella combobox
     *
     * @return
     */
    String getCombo();

    /**
     * aggiunge l'add crime listener
     *
     * @param addCrimeListener
     */
    void addAddCrimeListener(AddCrimeListener addCrimeListener);
}
