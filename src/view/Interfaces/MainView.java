package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;

import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.Main;

public class MainView extends PrisonManagerJFrame implements Main {

    /**
     *
     */
    private static final long serialVersionUID = -2585136897389059255L;

    private final PrisonManagerJPanel center;
    private final JButton addPrisoner = new JButton("Aggiungi prigioniero");
    private final JButton removePrisoner = new JButton("Rimuovi prigioniero");

    private final JButton viewPrisoner = new JButton("Vedi profilo prigioniero");
    private final PrisonManagerJPanel south;
    private final JButton highRankOnly = new JButton("Funzioni riservate (Grado 3)");
    private final JButton moreFunctions = new JButton("Altre funzioni (grado 2)");
    private final JButton logout = new JButton("Logout");
    private final PrisonManagerJPanel north;
    private final JLabel title = new JLabel("Prison Manager");

    /**
     * costruttore
     *
     * @param rank il rank della guardia che sta visualizzando il programma
     */
    public MainView(int rank) {
        this.setSize(550, 150);
        this.getContentPane().setLayout(new BorderLayout());
        center = new PrisonManagerJPanel(new FlowLayout());
        center.add(addPrisoner);
        center.add(removePrisoner);
        center.add(viewPrisoner);
        this.getContentPane().add(BorderLayout.CENTER, center);
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(moreFunctions);
        south.add(highRankOnly);
        south.add(logout);
        if (rank < 2) {
            moreFunctions.setEnabled(false);
        }
        if (rank < 3) {
            highRankOnly.setEnabled(false);
        }
        this.getContentPane().add(BorderLayout.SOUTH, south);
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(title);
        this.getContentPane().add(BorderLayout.NORTH, north);
        this.setVisible(true);
    }

    public void addLogoutListener(LogoutListener logoutListener) {
        logout.addActionListener((e) -> {
            MainView.this.dispose();
            logoutListener.onLogout();
        });
    }

    public void addInsertPrisonerListener(InsertPrisonerListener insertPrisonerListener) {
        addPrisoner.addActionListener((e) -> {
            MainView.this.dispose();
            insertPrisonerListener.onInsertPrisoner();
        });
    }

    public void addRemovePrisonerListener(RemovePrisonerListener removePrisonerListener) {
        removePrisoner.addActionListener((e) -> {
            MainView.this.dispose();
            removePrisonerListener.onRemovePrisoner();
        });
    }

    public void addViewPrisonerListener(ViewPrisonerListener viewPrisonerListener) {
        viewPrisoner.addActionListener((e) -> {
            MainView.this.dispose();
            viewPrisonerListener.onViewPrisoner();
        });
    }

    public void addMoreFunctionsListener(MoreFunctionsListener moreFListener) {
        moreFunctions.addActionListener((e) -> {
            MainView.this.dispose();
            moreFListener.onMoreFunctions();
        });
    }

    public void addSupervisorListener(SupervisorListener supervisorListener) {
        highRankOnly.addActionListener((e) -> {
            MainView.this.dispose();
            supervisorListener.onSupervisor();
        });
    }
}
