package controller.Interfaces;

public interface RemoveGuardController {

    /**
     * rimuovi una guardia dalla lista delle guardie
     */
    void removeGuard();
}
