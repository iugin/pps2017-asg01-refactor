package controller.Interfaces;

import model.Interfaces.Guard;

public interface LoginController {

    void login();

    void addOnLoginListener(final OnLoginListener onLoginListener);

    void logout();

    interface OnLoginListener {
        void onLogin(Guard guard);
    }
}
