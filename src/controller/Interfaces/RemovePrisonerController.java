package controller.Interfaces;

public interface RemovePrisonerController {

    /**
     * rimuove un prigioniero dalla lista dei prigionieri correnti
     */
    void removePrisoner();
}
