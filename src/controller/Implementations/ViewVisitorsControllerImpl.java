package controller.Implementations;

import java.util.List;

import javax.swing.JTable;

import controller.Interfaces.ViewVisitorsController;
import model.Interfaces.Visitor;
import model.datamanager.DataManagerFactory;
import view.Interfaces.Inter.ViewVisitors;
import view.Interfaces.MoreFunctionsView;

/**
 * controller della view visitors view
 *
 * @author Utente
 */
public class ViewVisitorsControllerImpl implements ViewVisitorsController {

    private static ViewVisitors viewVisitorsView;

    /**
     * costruttore
     *
     * @param viewVisitorsView la view
     */
    public ViewVisitorsControllerImpl(ViewVisitors viewVisitorsView) {
        ViewVisitorsControllerImpl.viewVisitorsView = viewVisitorsView;
        /*
         * listener che apre la view precedente
         */
        viewVisitorsView.addBackListener(() -> new MoreFunctionsControllerImpl(new MoreFunctionsView(viewVisitorsView.getRank())));
        viewVisitorsView.createTable(getTable());
    }

    public JTable getTable() {
        //creo una lista in cui inserisco i visitatori
        List<Visitor> list = DataManagerFactory.getVisitorsDataManager().getList();

        //metto gli elementi della lista in una matrice
        String[] vet = {"Nome", "Cognome", "Data di nascita", "ID prigioniero visitato"};
        String[][] mat = new String[list.size()][vet.length];
        for (int i = 0; i < list.size(); i++) {
            mat[i][0] = list.get(i).getName();
            mat[i][1] = list.get(i).getSurname();
            mat[i][2] = list.get(i).getBirthDate().toString();
            mat[i][3] = String.valueOf(list.get(i).getPrisonerID());
        }
        //creo la tabella passandogli la matrice
        JTable table = new JTable(mat, vet);
        return table;
    }

}
