package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Implementations.MovementImpl;
import model.datamanager.DataManagerFactory;
import view.Interfaces.AddMovementView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller che gestisce la addMovementView
 */
public class AddMovementControllerImpl {

    private static AddMovementView addMovementView;

    /**
     * costruttore
     *
     * @param addMovementView la view
     */
    public AddMovementControllerImpl(AddMovementView addMovementView) {
        AddMovementControllerImpl.addMovementView = addMovementView;
        AddMovementControllerImpl.addMovementView.addBackListener(new BackListener());
        AddMovementControllerImpl.addMovementView.addInsertListener(new InsertListener());
    }

    /**
     * torna alla view precedente
     */
    public class BackListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            addMovementView.dispose();
            new MoreFunctionsControllerImpl(new MoreFunctionsView(addMovementView.getRank()));
        }

    }

    /**
     * listener che inserisce un movimento
     */
    public static class InsertListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            //prendo il movimento dalla view
            MovementImpl m = new MovementImpl(addMovementView.getDesc(), addMovementView.getValue(), addMovementView.getSymbol().charAt(0));
            //se l'amount è inferiore a zero stampo l'errore
            if (m.getAmount() <= 0) {
                addMovementView.displayErrorMessage("Input invalido");
                return;
            }
            //aggiungo il nuovo movimento
            DataManagerFactory.getMovementsDataManager().add(m);
            addMovementView.displayErrorMessage("Movimento inserito");
        }
    }

}
	
	
	

