package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.Interfaces.InsertGuardController;
import model.Interfaces.Guard;
import model.datamanager.DataManagerFactory;
import view.Interfaces.InsertGuardView;
import view.Interfaces.SupervisorFunctionsView;

/**
 * controller che gestisce la insert guard view
 */
public class InsertGuardControllerImpl implements InsertGuardController {

    private static InsertGuardView insertGuardView;

    /**
     * costruttore
     *
     * @param insertGuardView la view
     */
    public InsertGuardControllerImpl(InsertGuardView insertGuardView) {
        InsertGuardControllerImpl.insertGuardView = insertGuardView;
        insertGuardView.addBackListener(new BackListener());
        insertGuardView.addInsertListener(new InsertListener());
    }

    /**
     * listener che fa tornare alla pagina precedente
     */
    public class BackListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            insertGuardView.dispose();
            new SupervisorControllerImpl(new SupervisorFunctionsView(insertGuardView.getRank()));
        }

    }

    public void insertGuard() {
        //recupero la guardia inserita nella view
        final Guard g = insertGuardView.getGuard();
        //controllo che non ci siano errori
        if (DataManagerFactory.getGuardsDataManager().getList(guard -> guard.getID() == g.getID()).size() > 0) {
            insertGuardView.displayErrorMessage("ID già usato");
        } else if (isSomethingEmpty(g)) {
            insertGuardView.displayErrorMessage("Completa tutti i campi correttamente");
        } else {
            //inserisco la guardia e salvo la lista aggiornata
            DataManagerFactory.getGuardsDataManager().add(g);
            insertGuardView.displayErrorMessage("Guardia inserita");

        }
    }

    /**
     * listener che si occupa di inserire una guardia
     */
    public class InsertListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            insertGuard();
        }

    }

    public boolean isSomethingEmpty(Guard g) {
        if (g.getName().equals("") || g.getSurname().equals("") || g.getRank() < 1 || g.getRank() > 3 || g.getID() < 0 || g.getPassword().length() < 6)
            return true;
        return false;
    }
}
