package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import controller.Interfaces.InsertPrisonerController;
import controller.factories.MainControllerFactory;
import model.Implementations.PrisonerImpl;
import model.Interfaces.Prisoner;
import model.datamanager.DataManagerFactory;
import view.Interfaces.InsertPrisonerView;

/**
 * controller della view insertprisoner
 */
public class InsertPrisonerControllerImpl implements InsertPrisonerController {

    private static InsertPrisonerView insertPrisonerView;

    /**
     * costruttore
     *
     * @param insertPrisonerView la view
     */
    public InsertPrisonerControllerImpl(InsertPrisonerView insertPrisonerView) {
        InsertPrisonerControllerImpl.insertPrisonerView = insertPrisonerView;
        insertPrisonerView.addInsertPrisonerListener(new InsertPrisonerListener());
        insertPrisonerView.addBackListener(new BackListener());
        insertPrisonerView.addAddCrimeListener(new AddCrimeListener());

    }

    public void insertPrisoner() {
        //recupero il prigioniero inserito nella view
        Prisoner p = new PrisonerImpl(insertPrisonerView.getName1(), insertPrisonerView.getSurname1(), insertPrisonerView.getBirth1(), insertPrisonerView.getPrisonerID1(), insertPrisonerView.getStart1(), insertPrisonerView.getEnd1(), insertPrisonerView.getList(), insertPrisonerView.getCellID());

        //controllo che non ci siano errori
        if (DataManagerFactory.getPrisonersDataManager().getList(prisoner -> prisoner.getIdPrigioniero() == p.getIdPrigioniero()).size() > 0) {
            insertPrisonerView.displayErrorMessage("ID già usato");
        } else if (isSomethingEmpty(p)) {
            insertPrisonerView.displayErrorMessage("Completa tutti i campi");
        } else {
            Calendar today = Calendar.getInstance();
            today.set(Calendar.HOUR_OF_DAY, 0); //
            if (p.getInizio().after(p.getFine()) || p.getInizio().before(today.getTime()) || p.getBirthDate().after(today.getTime())) {
                insertPrisonerView.displayErrorMessage("Correggi le date");
            } else {
                //aggiungo nuovo prigioniero in entrambe le liste
                DataManagerFactory.getPrisonersDataManager().add(p);
                DataManagerFactory.getCurrentPrisonersDataManager().add(p);
                if (DataManagerFactory.getCellsDataManager()
                        .getList(cell ->
                                (p.getCellID() == cell.getId() || p.getCellID() < 0 || p.getCellID() > 49) && cell.getCapacity() == cell.getCurrentPrisoners())
                        .size() > 0) {
                    insertPrisonerView.displayErrorMessage("Prova con un'altra cella");
                } else {
                    DataManagerFactory.getCellsDataManager().updateList(cell -> {
                        if (p.getCellID() == cell.getId()) {
                            cell.setCurrentPrisoners(cell.getCurrentPrisoners() + 1);
                        }
                        return cell;
                    });
                }
                insertPrisonerView.displayErrorMessage("Prigioniero inserito");
                insertPrisonerView.setList(new ArrayList<String>());
            }
        }
    }

    /**
     * listener che si occupa di inserire prigionieri
     */
    public class InsertPrisonerListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            insertPrisoner();
        }

    }

    /**
     * listener che fa tornare alla pagina precedente
     */
    public class BackListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            insertPrisonerView.dispose();
            MainControllerFactory.getMainController(insertPrisonerView.getRank());
        }

    }

    /**
     * listener che aggiunge un crimine alla lista dei crimini del prigioniero
     */
    public class AddCrimeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            //recupero i crimini inseriti nella view
            List<String> list = insertPrisonerView.getList();
            //controllo che il crimine inserito non fosse gia presente
            if (list.contains(insertPrisonerView.getCombo())) {
                insertPrisonerView.displayErrorMessage("Crimine già inserito");
            } else {
                //inserisco il crimine
                list.add(insertPrisonerView.getCombo());
                insertPrisonerView.setList(list);
            }
        }

    }

    public boolean isSomethingEmpty(Prisoner p) {
        if (p.getName().isEmpty() || p.getSurname().isEmpty() || p.getCrimini().size() == 1) {
            return true;
        }
        return false;
    }
}
