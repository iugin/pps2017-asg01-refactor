package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import controller.Interfaces.RemoveGuardController;
import model.Interfaces.Guard;
import model.datamanager.DataManagerFactory;
import view.Interfaces.RemoveGuardView;
import view.Interfaces.SupervisorFunctionsView;

/**
 * contoller della remove guard view
 */
public class RemoveGuardControllerImpl implements RemoveGuardController {

    private static RemoveGuardView removeGuardView;

    /**
     * costruttore
     *
     * @param removeGuardView la view
     */
    public RemoveGuardControllerImpl(RemoveGuardView removeGuardView) {
        RemoveGuardControllerImpl.removeGuardView = removeGuardView;
        removeGuardView.addBackListener(new BackListener());
        removeGuardView.addRemoveGuardListener(new RemoveGuardListener());
    }

    /**
     * listener che si occupa della rimozione della guardia
     */
    public class RemoveGuardListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            removeGuard();
        }


    }

    public void removeGuard() {
        List<Guard> guards = DataManagerFactory.getGuardsDataManager()
                .getList(guard -> guard.getID() == removeGuardView.getID());
        if (guards.size() > 0) {
            guards.forEach(guard -> {
                DataManagerFactory.getGuardsDataManager().remove(guard);
                removeGuardView.displayErrorMessage("Guardia rimossa");
            });
        } else {
            removeGuardView.displayErrorMessage("Guardia non trovata");
        }
    }

    /**
     * listener che apre la view precedente
     */
    public static class BackListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            removeGuardView.dispose();
            new SupervisorControllerImpl(new SupervisorFunctionsView(removeGuardView.getRank()));
        }

    }
}
