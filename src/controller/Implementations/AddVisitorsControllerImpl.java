package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Interfaces.Visitor;
import model.datamanager.DataManagerFactory;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller della addVisitorsView
 */
public class AddVisitorsControllerImpl {

    private static AddVisitorsView visitorsView;

    /**
     * costruttore
     *
     * @param view la view
     */
    public AddVisitorsControllerImpl(AddVisitorsView view) {
        AddVisitorsControllerImpl.visitorsView = view;
        visitorsView.addBackListener(new BackListener());
        visitorsView.addInsertVisitorListener(new InsertListener());
    }

    /**
     * listener che fa tornare alla pagina precedente
     */
    public class BackListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            visitorsView.dispose();
            new MoreFunctionsControllerImpl(new MoreFunctionsView(visitorsView.getRank()));
        }

    }

    /**
     * listener che gestisce l'inserimento di visitatori
     */
    public static class InsertListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            //salvo il visitatore inserito nella view
            Visitor vi = visitorsView.getVisitor();
            if (vi.getName().length() < 2 || vi.getSurname().length() < 2 || !checkPrisonerID(vi))
                visitorsView.displayErrorMessage("Devi inserire un nome, un cognome e un prigioniero esistente");
            else {
                //inserisco il visitatore nella lista
                DataManagerFactory.getVisitorsDataManager().add(vi);
                visitorsView.displayErrorMessage("Visitatore inserito");
            }
        }

        /**
         * controlla se l'id del prigioniero inserita è corretta
         *
         * @param visitor visitatore
         * @return true se l'id è corretto
         */
        public static boolean checkPrisonerID(final Visitor visitor) {
            return DataManagerFactory.getCurrentPrisonersDataManager().getList(prisoner -> prisoner.getIdPrigioniero() == visitor.getPrisonerID()).size() > 0;
        }
    }
}



