package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import controller.Interfaces.ViewPrisonerController;
import controller.factories.MainControllerFactory;
import model.Interfaces.Prisoner;
import model.datamanager.DataManagerFactory;
import view.Interfaces.ViewPrisonerView;

/**
 * controller della view prisoner view
 */
public class ViewPrisonerControllerImpl implements ViewPrisonerController {

    private static ViewPrisonerView viewPrisonerView;

    /**
     * costruttore
     *
     * @param viewPrisonerView la view
     */
    public ViewPrisonerControllerImpl(ViewPrisonerView viewPrisonerView) {
        ViewPrisonerControllerImpl.viewPrisonerView = viewPrisonerView;
        viewPrisonerView.addViewListener(new ViewProfileListener());
        viewPrisonerView.addBackListener(new BackListener());
    }

    /**
     * listener che si occupa di far mostrare il prigioniero
     */
    public class ViewProfileListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            showPrisoner();
        }

    }

    public void showPrisoner() {
        if (String.valueOf(viewPrisonerView.getID()).isEmpty()) {
            viewPrisonerView.displayErrorMessage("Devi inserire un ID");
        } else {
            final List<Prisoner> prisoners = DataManagerFactory.getPrisonersDataManager().getList(prisoner ->
                    prisoner.getIdPrigioniero() == viewPrisonerView.getID());
            if (prisoners.size() > 0) {
                prisoners.forEach(prisoner -> {
                    //quando l'id corrisponde stampo i dati del prigioniero
                    viewPrisonerView.setProfile(prisoner.getName(), prisoner.getSurname(), prisoner.getBirthDate().toString(), prisoner.getInizio().toString(), prisoner.getFine().toString());
                    viewPrisonerView.setTextArea(prisoner.getCrimini());
                });
            } else {
                viewPrisonerView.displayErrorMessage("Prigioniero non trovato");
            }
        }
    }

    /**
     * listener che apre la view precedente
     */
    public static class BackListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            viewPrisonerView.dispose();
            MainControllerFactory.getMainController(viewPrisonerView.getRank());
        }

    }
}
