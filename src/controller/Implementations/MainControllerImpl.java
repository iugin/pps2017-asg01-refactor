package controller.Implementations;

import controller.factories.LoginFactory;
import view.Interfaces.InsertPrisonerView;
import view.Interfaces.Inter.Main;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.RemovePrisonerView;
import view.Interfaces.SupervisorFunctionsView;
import view.Interfaces.ViewPrisonerView;

/**
 * controller dela main view
 */
public class MainControllerImpl {

    /**
     * costruttore
     *
     * @param mainView la view
     */
    public MainControllerImpl(final Main mainView, final int guardRank) {

        /*
         * listener che fa tornare alla login view
         */
        mainView.addLogoutListener(() ->
                LoginFactory.getLoginController().logout()
        );

        /*
         * listener che fa andare alla insert prisoner view
         */
        mainView.addInsertPrisonerListener(() ->
                new InsertPrisonerControllerImpl(new InsertPrisonerView(guardRank))
        );

        /*
         * listener che fa andare alla remove prisoner view
         */
        mainView.addRemovePrisonerListener(() ->
                new RemovePrisonerControllerImpl(new RemovePrisonerView(guardRank))
        );

        /*
         * listener che fa andare alla view prisoner view
         */
        mainView.addViewPrisonerListener(() ->
                new ViewPrisonerControllerImpl(new ViewPrisonerView(guardRank))
        );

        /*
         * listener che fa andare alla more functions view
         */
        mainView.addMoreFunctionsListener(() ->
                new MoreFunctionsControllerImpl(new MoreFunctionsView(guardRank))
        );

        /*
         * listener che fa andare alla supervisoner functions view
         */
        mainView.addSupervisorListener(() ->
                new SupervisorControllerImpl(new SupervisorFunctionsView(guardRank))
        );
    }

}
