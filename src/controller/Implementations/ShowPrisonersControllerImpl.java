package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.JTable;

import model.Interfaces.Prisoner;
import model.datamanager.DataManagerFactory;
import view.Components.PrisonManagerJFrame;
import view.Interfaces.ShowPrisonersView;
import view.Interfaces.SupervisorFunctionsView;

/**
 * controller che gestisce la show prisoners view
 */
public class ShowPrisonersControllerImpl extends PrisonManagerJFrame {

    /**
     *
     */
    private static final long serialVersionUID = -2056633481557914162L;

    private static ShowPrisonersView showPrisonersView;

    /**
     * controlle
     *
     * @param showPrisonersView la view
     */
    public ShowPrisonersControllerImpl(ShowPrisonersView showPrisonersView) {
        ShowPrisonersControllerImpl.showPrisonersView = showPrisonersView;
        ShowPrisonersControllerImpl.showPrisonersView.addBackListener(new BackListener());
        ShowPrisonersControllerImpl.showPrisonersView.addComputeListener(new ComputeListener());
    }

    /**
     * listener che apre la view precedente
     */
    public static class BackListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            showPrisonersView.dispose();
            new SupervisorControllerImpl(new SupervisorFunctionsView(showPrisonersView.getRank()));
        }

    }

    /**
     * crea una tabella contenente i prigionieri che tra le due date prese in input erano in prigione
     */
    public static class ComputeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            //salvo le date inserite nella view
            Date from = showPrisonersView.getFrom();
            Date to = showPrisonersView.getTo();
            //recupero la lista dei prigionieri
            List<Prisoner> prisoners = DataManagerFactory.getPrisonersDataManager().getList(prisoner ->
                    prisoner.getInizio().before(to) && prisoner.getFine().after(from));
            //creo una matrice con i prigionieri inclusi
            JTable table = new JTable();
            String[] vet = {"id", "nome", "cognome", "giorno di nascita", "inizio prigionia", "fine prigionia"};
            String[][] mat = new String[prisoners.size()][vet.length];
            for (int i = 0; i < prisoners.size(); i++) {
                mat[i][0] = String.valueOf(prisoners.get(i).getIdPrigioniero());
                mat[i][1] = prisoners.get(i).getName();
                mat[i][2] = prisoners.get(i).getSurname();
                mat[i][3] = prisoners.get(i).getBirthDate().toString();
                mat[i][4] = prisoners.get(i).getInizio().toString();
                mat[i][5] = prisoners.get(i).getFine().toString();
            }
            //creo la tabella passandogli i dati della matrice
            table = new JTable(mat, vet);
            showPrisonersView.createTable(table);
        }

    }
}
