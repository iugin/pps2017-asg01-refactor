package controller.Implementations;

import java.util.List;
import java.util.Optional;

import controller.Interfaces.LoginController;
import model.Interfaces.Guard;
import model.datamanager.DataManagerFactory;
import view.Interfaces.LoginView;

/**
 * controller della login view
 */
public class LoginControllerImpl implements LoginController {

    private LoginView loginView;

    /**
     * costruttore
     *
     * @param loginView la view
     */
    public LoginControllerImpl(LoginView loginView) {
        this.loginView = loginView;

    }

    private Optional<Guard> login(String username, String password) {
        List<Guard> guards = DataManagerFactory.getGuardsDataManager().getList(guard ->
                username.equals(String.valueOf(guard.getUsername())) && password.equals(guard.getPassword()));
        if (guards.size() > 0) {
            return Optional.of(guards.get(0));
        }
        return Optional.empty();
    }

    @Override
    public void login() {
        displayLoginPanel();
    }

    @Override
    public void addOnLoginListener(OnLoginListener onLoginListener) {
        loginView.addLoginListener((username, password) -> {
            if (username.isEmpty() || password.isEmpty()) {
                loginView.displayErrorMessage("Devi inserire username e password");
            }
            final Optional<Guard> guard = login(username, password);
            if (guard.isPresent()) {
                //effettuo il login
                loginView.displayErrorMessage("Benvenuto Utente " + username);
                loginView.dispose();
                onLoginListener.onLogin(guard.get());
            } else {
                loginView.displayErrorMessage("Combinazione username/password non corretta");
            }
        });
    }

    @Override
    public void logout() {
        displayLoginPanel();
    }

    private void displayLoginPanel() {
        loginView.reset();
        loginView.setVisible(true);
        loginView.displayErrorMessage("accedere con i profili: \n id:3 , password:qwerty \n id:2 , password:asdasd ");
    }
}
