package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import controller.Interfaces.RemovePrisonerController;
import controller.factories.MainControllerFactory;
import model.Interfaces.Prisoner;
import model.datamanager.DataManagerFactory;
import view.Interfaces.RemovePrisonerView;

/**
 * controller della remove prisoner view
 */
public class RemovePrisonerControllerImpl implements RemovePrisonerController {

    private static RemovePrisonerView removePrisonerView;

    /**
     * costruttore
     *
     * @param removePrisonerView la view
     */
    public RemovePrisonerControllerImpl(RemovePrisonerView removePrisonerView) {
        RemovePrisonerControllerImpl.removePrisonerView = removePrisonerView;
        removePrisonerView.addRemoveListener(new RemoveListener());
        removePrisonerView.addBackListener(new BackListener());
    }

    /**
     * listener che si occupa di rimuovare il prigioniero
     */
    public class RemoveListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            removePrisoner();
        }


    }

    public void removePrisoner() {
        List<Prisoner> prisoners = DataManagerFactory.getCurrentPrisonersDataManager()
                .getList(prisoner -> prisoner.getIdPrigioniero() == removePrisonerView.getID());
        if (prisoners.size() > 0) {
            prisoners.forEach(prisoner -> {
                DataManagerFactory.getCurrentPrisonersDataManager().remove(prisoner);
                DataManagerFactory.getCellsDataManager().updateList(cell -> {
                    if (prisoner.getCellID() == cell.getId()) {
                        //diminuisco il numero di prigionieri correnti nella cella del prigioniero rimosso
                        cell.setCurrentPrisoners(cell.getCurrentPrisoners() - 1);
                    }
                    return cell;
                });
                removePrisonerView.displayErrorMessage("Prigioniero rimosso");
            });
        } else {
            removePrisonerView.displayErrorMessage("Prigioniero non trovato");
        }
    }

    /**
     * listener che apre la view precedente
     */
    public class BackListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            removePrisonerView.dispose();
            MainControllerFactory.getMainController(removePrisonerView.getRank());
        }

    }


}