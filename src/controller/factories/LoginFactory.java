package controller.factories;

import controller.Implementations.LoginControllerImpl;
import controller.Interfaces.LoginController;
import view.Interfaces.LoginView;

public class LoginFactory {

    private static final LoginController loginController = new LoginControllerImpl(new LoginView());

    private LoginFactory() {}

    public static LoginController getLoginController() {
        return loginController;
    }
}
