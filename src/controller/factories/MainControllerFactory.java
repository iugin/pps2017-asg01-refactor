package controller.factories;

import controller.Implementations.MainControllerImpl;
import view.Interfaces.MainView;

public class MainControllerFactory {

    public static MainControllerImpl getMainController(final int guardRank) {
        return new MainControllerImpl(new MainView(guardRank), guardRank);
    }
}
