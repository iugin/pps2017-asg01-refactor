package main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import controller.factories.LoginFactory;
import controller.factories.MainControllerFactory;
import model.Implementations.CellImpl;
import model.Implementations.GuardBuilder;
import model.Implementations.GuardImpl;
import model.Interfaces.Cell;
import model.Interfaces.Guard;
import model.datamanager.DataManagerFactory;

/**
 * The main of the application.
 */
public final class Main {

    /**
     * Program main, this is the "root" of the application.
     *
     * @param args unused,ignore
     */
    public static void main(final String... args) {
        if (DataManagerFactory.getGuardsDataManager().getList().isEmpty()) {
            initializeGuards();
        }
        if (DataManagerFactory.getCellsDataManager().getList().isEmpty()) {
            initializeCells();
        }

        //chiamo controller e view del login
        LoginFactory.getLoginController().addOnLoginListener(guard -> MainControllerFactory.getMainController(guard.getRank()));
        LoginFactory.getLoginController().login();
    }

    /**
     * metodo che inizializza le celle
     */
    private static void initializeCells() {

        List<Cell> list = new ArrayList<>();
        CellImpl c;
        for (int i = 0; i < 50; i++) {
            if (i < 20) {
                c = new CellImpl(i, "Primo piano", 4);
            } else if (i < 40) {
                c = new CellImpl(i, "Secondo piano", 3);
            } else if (i < 45) {
                c = new CellImpl(i, "Terzo piano", 4);
            } else {
                c = new CellImpl(i, "Piano sotterraneo, celle di isolamento", 1);
            }
            list.add(c);
        }
        DataManagerFactory.getCellsDataManager().addAll(list);
    }

    /**
     * metodo che inizializza le guardie
     */
    private static void initializeGuards() {

        String pattern = "MM/dd/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = format.parse("01/01/1980");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Guard> list = new ArrayList<>();
        GuardImpl g1 = new GuardBuilder()
                .setName("Oronzo")
                .setSurname("Cantani")
                .setBirthDate(date)
                .setRank(1)
                .setTelephoneNumber("0764568")
                .setIdGuardia(1)
                .setPassword("ciao01")
                .createGuardImpl();
        list.add(g1);
        GuardImpl g2 = new GuardBuilder()
                .setName("Emile")
                .setSurname("Heskey")
                .setBirthDate(date)
                .setRank(2)
                .setTelephoneNumber("456789")
                .setIdGuardia(2)
                .setPassword("asdasd")
                .createGuardImpl();
        list.add(g2);
        GuardImpl g3 = new GuardBuilder()
                .setName("Gennaro")
                .setSurname("Alfieri")
                .setBirthDate(date)
                .setRank(3)
                .setTelephoneNumber("0764568")
                .setIdGuardia(3)
                .setPassword("qwerty")
                .createGuardImpl();
        list.add(g3);
        DataManagerFactory.getGuardsDataManager().addAll(list);
    }

}
